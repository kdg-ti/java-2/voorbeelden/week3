import java.util.*;

public class DemoVarargs {
    public static void main(String[] args) {
        List<String> myList1 = new ArrayList<>();
        List<String> myList2 = new ArrayList<>();
        List<String> myList3 = new LinkedList<>();

        myList1.add("Alfa"); myList1.add("Bravo");  myList1.add("Charlie");
        myList2.add("Delta"); myList2.add("Echo");
        myList3.add("Zoeloe");

        printLists(myList1, myList2, myList3);
    }

    @SafeVarargs
    public static void printLists(List<String>... stringLists) {
        String firstString = stringLists[0].get(0);
        List<String> lastList = stringLists[stringLists.length - 1];
        String lastString = lastList.get(lastList.size() - 1);
        System.out.println("firstString = " + firstString);
        System.out.println("lastString = " + lastString);
    }
}